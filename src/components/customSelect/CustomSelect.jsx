import React, { useState,useContext } from 'react';
import styles from './CustomSelect.module.css';
import { MdExpandLess, MdExpandMore } from 'react-icons/md';
import ThemeContext from '../theme/Theme';


// This declares a functional component named Custom which takes three props: 
// options (an array of options to display), 
// handleSelect (a function to handle selection of an option), 
// and filterBy (a string indicating what type of filter is applied).

const Custom = ({ options, handleSelect,filterBy }) => {

// Two state variables are declared using the useState hook.
// isOpen tracks whether the dropdown menu is open or closed.

  const [isOpen, setIsOpen] = useState(false);

  // selectedOption tracks the currently selected option.
  const [selectedOption, setSelectedOption] = useState(null);

  // It is used to theme dark(or) light
  const { darkMode, setDarkMode } = useContext(ThemeContext);

  // toggleDropdown is a function that toggles the isOpen state, changing the visibility of the dropdown menu.
  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  // handleOptionClick is a function that updates the selectedOption, 
  // closes the dropdown menu, and 
  // calls the handleSelect function with the selected option as an argument.
  const handleOptionClick = (option) => {
    setSelectedOption(option);
    setIsOpen(false);
    handleSelect(option)
  };

  return (
    <div className={styles.filterContainer}>
      <div
        className={`${styles.dropdownToggle} ${isOpen ? styles.open : ''}`}
        onClick={toggleDropdown}
        style={{ backgroundColor: darkMode ? '#2B3743' : 'white', color: darkMode ? 'white' : 'black' }}
      >
        {selectedOption ? filterBy === 'subregion' ? selectedOption : selectedOption.sortType : `Sort by ${filterBy}`}
        <span className={styles.arrow}>
          {isOpen ? <MdExpandMore /> : <MdExpandLess />}
        </span>
      </div>

      {/* For the list container (<p> element), the background color is set to a dark shade (#202D36) if darkMode is true, 
        otherwise to a light shade (#FAFAFA). Text color is set to white or black based on darkmode */}
      <p className={`${styles.dropdownMenu} ${isOpen ? styles.open : ''}`}
        style={{ backgroundColor: darkMode ? '#2B3743' : 'white', color: darkMode ? 'white' : 'black' }}
      >
        {options && options.map((option, index) => (

      //  For each country, a list item (<p>) is created.
       // Each list item has its own styling applied based on darkmode
          <p key={index} onClick={() => handleOptionClick(option)}
            style={{ backgroundColor: darkMode ? '#2B3743' : 'white', color: darkMode ? 'white' : 'black' }}>

            {filterBy === 'subregion' ? option : option.sortType}
          </p>
        ))}
      </p>
    </div>
  );
};

export default Custom;



