import React from 'react';
import Custom from '../customSelect/CustomSelect';

const FilterSubregion = ({ allSubRegions, updateCountriesBySubregion, darkMode }) => {

    function handleSelect(option) {
        updateCountriesBySubregion(option)
    }
    return (
        <div className="app">
            
            <Custom options={allSubRegions} handleSelect={handleSelect} darkMode={darkMode} filterBy='subregion' />

        </div>
    );
};

export default FilterSubregion;



