import React,{useContext} from 'react';
import styles from './Header.module.css';
import { BsMoon } from 'react-icons/bs';
import ThemeContext from '../theme/Theme';


function Header({  }) {
    const { darkMode, setDarkMode } = useContext(ThemeContext);

    // Toggleing the light mode to dark mode
    
    function toggleDarkMode(){
        setDarkMode(!darkMode)
    }
    return (
        <div className={styles.headerContainer}
            style={{ backgroundColor: darkMode ? '#2B3743' : 'white', color: darkMode ? 'white' : 'black', boxShadow: !darkMode && '0px 4px 10px rgba(43, 43, 43, 0.2)'}}
        >
            <h1 className={styles.world}>Where in the world?</h1>
            <div className={styles.headerMode}>
                <p className={styles.modeText} onClick={toggleDarkMode}>
                    <span className={styles.moonIcon}>
                        <BsMoon />
                    </span>
                    Dark Mode
                </p>
            </div>
        </div>
   );
}

export default Header;
