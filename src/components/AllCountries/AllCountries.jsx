import React,{useContext} from 'react';
import countryStyle from './AllCountries.module.css';
import ThemeContext from '../theme/Theme';

// This declares a functional component named AllCountries.
// It takes two props as arguments: "countries" which is an array of country data and 
// "searchQuery" which is a string used for filtering countries.

function AllCountries({ countries, searchQuery}) {

    // it is used to set the theme
    const { darkMode, setDarkMode } = useContext(ThemeContext);

    // This filters the array of countries based on the searchQuery. 
    // It converts both the country name and the search query to lowercase for a case-insensitive comparison.
    const filteredCountries = countries.filter((country) => {
        return country.name.common.toLowerCase().includes(searchQuery.toLowerCase());
    });

   
    return (
        <>

        {/* This code conditionally renders JSX based on whether any countries match the search query. 
        If no countries match, it renders a message saying "Not found", otherwise, it renders a list of countries. */}

            {filteredCountries.length === 0 ?
                <p className={countryStyle.notFound}
                    style={{ color: darkMode ? 'white' : 'black' }}> Countries not Found
                </p> :

                // For the list container (<ul> element), the background color is set to a dark shade (#202D36) if darkMode is true, 
                 //   otherwise to a light shade (#FAFAFA). Text color is set to white or black based on darkMode.
                <p className={countryStyle.countriesContainer}
                    style={{ backgroundColor: darkMode ? '#202D36' : '#FAFAFA', color: darkMode ? 'white' : 'black' }}>

                    {filteredCountries.map((country, index) => (
                        // For each country, a list item (<li>) is created.
                        // Each list item has its own styling applied based on darkMode.
                        <p key={index} className={countryStyle.eachCountry}
                            style={{ backgroundColor: darkMode ? '#2B3743' : 'white', color: darkMode ? 'white' : 'black' }}>
                                

                        {/* This div is used to print the country images */}
                            <div className={countryStyle.countryImgContainer}>
                                <img
                                    src={country.flags.png}
                                    alt={country.flags.alt}
                                    className={countryStyle.countryImg}
                                />
                            </div>

                            {/* This div is used to print the country name */}
                            <div className={countryStyle.countryDetails}>
                                <div className={countryStyle.countryName}>
                                    <p>{country.name.common.slice(0, 35)}</p>
                                </div>

                                {/* This div is used to print the description of the image */}
                                <div className={countryStyle.countryInfo}>
                                    <p>Population: {country.population}</p>
                                    <p>Region: {country.region}</p>
                                    <p>Capital: {country.capital} </p>
                                    <p>Sub-region: {country.subregion}</p>
                                    <p>Area: {country.area}</p>
                                </div>
                            </div>
                        </p>
                    ))}
                </p>
            }
        </>
    );
}

export default AllCountries;
