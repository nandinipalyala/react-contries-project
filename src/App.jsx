import React, { useEffect, useState,useContext } from 'react';
import appStyles from './App.module.css';
import axios from 'axios';
import Header from './components/Header/Header.jsx';
import Search from './components/search/Search';
import Filter from './components/filter/Filter';
import AllCountries from './components/AllCountries/AllCountries.jsx';
import FilterSubregion from './components/filterSubregion/FilterSubregion.jsx';
import SortPopulation from './components/sortByPopulation/SortPopulation';
import SortArea from './components/sortByArea/SortArea';
import ThemeContext from './components/theme/Theme';


// writing the state variables using react hook "useState"
function App() {
  const [searchQuery, setSearchQuery] = useState('');
  const [countries, setCountries] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [selectedRegion, setSelectedRegion] = useState(null);
  const [selectedSubregion, setSelectedSubregion] = useState(null);
  const [population, setPopulation] = useState(null);
  const [area, setArea] = useState(null);
  const { darkMode, setDarkMode } = useContext(ThemeContext);

  useEffect(() => {

    // fetching the API using "axios"
    axios
      .get('https://restcountries.com/v3.1/all')
      .then((response) => {
        if (response.status !== 200) {
          console.log('Fetching data failed');
          setError(true); // it will check the error is occured during fetching the data
          setLoading(false); // It is used to see the data fetched or not
          throw new Error('Fetching data failed');
        } else {
          return response.data;
        }
      })
      .then((data) => {
        if (data.length === 0) {
          setLoading(false);
          setError(false);
        } else {
          setCountries(data);
          setLoading(false);
          setError(false);
        }
      })
      .catch((err) => {
        setError(true);
        setLoading(false);
        console.error(err, 'Fetching error');
      });
  }, []);

// Updating the regions

// it will check and update the selected regions for all countries
  const updateCountriesByRegion = (region) => {
    setSelectedRegion(region);
    if (selectedRegion === 'All') {
      setSelectedSubregion(null);
    }
  };

  // it will check the sub-regions are updated or not

  const updateCountriesBySubregion = (subregion) => {
    setSelectedSubregion(subregion);
  };

// This code for using  sort by region and subregion

  const allSubRegions = countries.reduce((acc, current) => {
    if (!acc[current.region]) {
      acc[current.region] = [];
    }
    if (current.subregion && !acc[current.region].includes(current.subregion)) {
      acc[current.region].push(current.subregion);
    }

    return acc;
  }, {});



// filter region and subregion

  let filteredCountries;

  filteredCountries = countries.filter((country) => {
    if (selectedRegion === 'All') {
      return true;
    }
    if (selectedRegion && selectedSubregion) {
      return country.region === selectedRegion && country.subregion === selectedSubregion;

    } else if (selectedRegion) {

      return country.region === selectedRegion;
    } else if (selectedSubregion) {
      return country.subregion === selectedSubregion;
    }
    return true;
  });


  // Sorting by population and area using sort() method
  filteredCountries = filteredCountries.sort((first, second) => {

    // filtering the population and area
    if(population === 'All' || area === 'All') {
      return 0;
    }

    // sorting the population 
    if (population === 'descending') {
      return second.population - first.population;
    } else if (population === 'ascending') {
      return first.population - second.population;

    // sorting the area
    } else if (area === 'ascending') {
      return first.area - second.area;
    } else if (area === 'descending') {
      return second.area - first.area;
    }
  });


// funtion by using to sort the population
  function sortPopulation(type) {
    setPopulation(type);
    setArea(null);
    // console.log(population, "population")

  }
// funtion by using to sort the area
  function sortArea(type) {
    setArea(type);
    setPopulation(null);
    // console.log(area, "area");
  }

//HEADER PART
  return (
    <div>
      <Header />

      {/* this div is used to search the countries */}
      <div className={appStyles.searchFilter}
        style={{ backgroundColor: darkMode ? '#202D36' : '#FAFAFA', color: darkMode ? 'white' : 'black' }}
      >

        {/*searchfilter */}
        <Search setSearchQuery={setSearchQuery}  />

        {/*sorting area */}
        <SortArea sortArea={sortArea} />

        {/*sorting population */}
        <SortPopulation  sortPopulation={sortPopulation} />  

        {selectedRegion !== 'All' && <FilterSubregion
          allSubRegions={selectedRegion && allSubRegions[selectedRegion]}
          updateCountriesBySubregion={updateCountriesBySubregion}
          
        />}

        <Filter updateCountriesByRegion={updateCountriesByRegion}/>

      </div>
      <section>

        {/* This is for the error message  if the data is not found it will show the error meaasge */}
        {loading && <div className={appStyles.ldsDualRing}></div>}
        {error && (
          <div className={appStyles.errorMessage}>
            <h1>Fetching the API Rest-countries data is not found.</h1>
          </div>
        )}

         {/* This code is used to write the country functionalities which will be shown on the page. */}
         {!loading && (selectedRegion || selectedSubregion || population || area) ? (
          <AllCountries
            countries={filteredCountries}
            searchQuery={searchQuery}
            darkMode={darkMode}
          />

          // This is for the error message
        ) : !loading && !error && !selectedRegion && (
          <AllCountries
            countries={countries}
            searchQuery={searchQuery}
            darkMode={darkMode}
          />
        )}
      </section>
    </div>
  );
}

export default App;
